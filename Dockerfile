FROM openjdk:16-slim-buster

COPY requirements.txt ./

# Install Python
RUN apt-get update && apt-get install --yes python3 python3-pip

# Install dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/opt/model_fitter_two_class_svm/src/"

COPY src /opt/model_fitter_two_class_svm/src/
ENTRYPOINT ["python3", "/opt/model_fitter_two_class_svm/src/main.py"]
