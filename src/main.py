#!/usr/bin/env python3

import logging
import pickle
import uuid

import pandas as pd
from learnfram.application.Application import Application
from learnfram.services.ModelComponent import ModelComponent
from sklearn.svm import SVC


class Service(ModelComponent):

    component_type = "model_fitter_two_class_svm"

    def run(self):
        logging.info(f"Starting...")

        train = pd.DataFrame.from_dict(self.data["train"]["data"])
        annotation = pd.DataFrame.from_dict(self.data["train"]["annotation"])

        svm = SVC()

        svm.fit(train, annotation)
        model_name = "OneClassSVM_{}.sav".format(uuid.uuid1())

        logging.info(f"The model {model_name} will be created")

        model = pickle.dumps(svm)

        logging.info(f"Model created")

        self.send(model_name, model)


if __name__ == "__main__":
    Application.start(Service)
